
@extends('dashboard')
@section('content')

@include('modal.modalCliente')
<h2>Clientes</h2>
  <div class="table-responsive">
    <table class="table table-striped table-sm">
      <thead>
        <tr>
          <th scope="col">Nombre</th>
          <th scope="col">apellido</th>
          <th scope="col">cedula</th>
          <th scope="col">tipo</th>
          <th scope="col">Direccion</th>
          <th scope="col">acciones</th>
        </tr>
      </thead>
      @foreach( $cliente as $clientes)
      <tbody>
        <tr>
          <td>{{$clientes->nombre}}</td>
          <td>{{$clientes->apellido}}</td>
          <td>{{$clientes->cedula}}</td>
          <td>{{$clientes->tipo}}</td>
          <td>{{$clientes->direccion}}</td>
          <td>

            <button type="button" class="btn btn-danger btn-sm">eliminar</button>
            <button type="button" class="btn btn-info btn-sm">ver</button>
            <button type="button" class="btn btn-warning btn-sm">modificar</button>
          </td>
        </tr>



      </tbody>
      @endforeach
    </table>
  </div>
@endsection
