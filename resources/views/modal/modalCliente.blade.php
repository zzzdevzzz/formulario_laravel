 
<button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#cliente">
  Crear
</button>
<h2>Clientes</h2>
  <div class="table-responsive">
    <table class="table table-striped table-sm">
      <thead>
        <tr>
          <th scope="col">Nombre</th>
          <th scope="col">apellido</th>
          <th scope="col">cedula</th>
          <th scope="col">tipo</th>
          <th scope="col">Direccion</th>
          <th scope="col">acciones</th>
        </tr>
      </thead>
      @foreach( $cliente as $clientes)
      <tbody>
        <tr>
          <td>{{$clientes->nombre}}</td>
          <td>{{$clientes->apellido}}</td>
          <td>{{$clientes->cedula}}</td>
          <td>{{$clientes->tipo}}</td>
          <td>{{$clientes->direccion}}</td>
          <td>

            <button type="button" class="btn btn-danger btn-sm">eliminar</button>
            <button type="button" class="btn btn-info btn-sm">ver</button>
            <button type="button" class="btn btn-warning btn-sm">modificar</button>
          </td>
        </tr>



      </tbody>
      @endforeach
    </table>
  </div>
 
 <!-- Modal -->
  <div class="modal fade  " id="cliente" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog cuadro1">
      <div class="modal-content ">
        <div class="modal-header cuadro1 ">
          <h5 class="modal-title" id="exampleModalLabel">Descripción Cliente</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div> 
        <div class="modal-body cuadro">
          <form class="form-inline"  action="{{ route('clientes.store') }}" method="POST">
            @csrf
            <div class="form-group">
              <label for="email"></label>
              <input type="text" class="form-control" id="nombre " name="nombre" placeholder="Nombre">
            </div>
            <div class="form-group">
              <label for="pwd"></label>
              <input type="text" class="form-control" id="apellido"   name="apellido" placeholder="Apellido">
            </div>
            <div class="form-group">
                <label for="pwd"></label>
                <input type="text" class="form-control" id="cedula"   name="cedula" placeholder="cedula">
              </div>
            <div class="form-group">
              <label for="pwd"></label>
              <input type="text" class="form-control" id="tipo"    name="tipo" placeholder="Tipo">
            </div>
            <div class="mb-3">
              <label for="pwd"></label>
              <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"  name="direccion" placeholder="Direccion "></textarea>
            </div>
            <button type="submit"  class="btn btn-secondary">Save</button>
          </form>
        </div>
     
      </div>
    </div>


    <style>


      .cuadro{
      
        background: #0F2027;  /* fallback for old browsers */
      background: -webkit-linear-gradient(to right, #2C5364, #203A43, #0F2027);  /* Chrome 10-25, Safari 5.1-6 */
      background: linear-gradient(to right, #2C5364, #203A43, #0F2027); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
      }
      
      .cuadro1{
        background: #0F2027;  /* fallback for old browsers */
      background: -webkit-linear-gradient(to right, #2C5364, #203A43, #0F2027);  /* Chrome 10-25, Safari 5.1-6 */
      background: linear-gradient(to right, #2C5364, #203A43, #0F2027); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
      
      }
      
      
      </style>